# PyNorm - Python Normalizer
A Sublime Text plugin to normalize identation for a python source file.

# Install

## Sublime Text 3 Package Control

Install via the package control under https://packagecontrol.io/ . Search for "PyNorm"

## Manual

You can also install it by cloning the repo into your Packages folder, but the installation via Package Control is recommended

## Usage

It's pretty straight forward to use either choose "Tools" -> "PyNorm" or use the shortcut (default Ctrl+Alt+f). There are also options to let it run automatically on load.

The plugin uses the setttings from the language definition file to determine if to use tabs or spaces and which width is wanted. If you don't want the settings from the default settings you can define them in the language user settings or your global user settings.
